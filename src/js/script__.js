$(document).ready(function(){
    console.log("ola")
    
    $.ajax({
        method: "GET",
        url: "https://dados-ufma.herokuapp.com/subunidade/1396/docentes",
         })
        .done( ( docentes) => {
          let divDocente = $(".docente")
          let divDocentes = $("#corpo-docente")
          
          for (let docente of docentes) {
              let urlDoc = "https://dados-ufma.herokuapp.com/docente/"+docente.siape

              let div = divDocente.clone()
              $.ajax({
                method : "GET",
                url: urlDoc,
              }).done ( (reponse )  => {
              
                let detail =  {nome: docente.nome, siape: docente.siape, 
                  img: reponse.urlimg, email: reponse.email,
                  formacao: reponse.formacao
                }

                div.find("#foto-docente").eq(0).append (
                  $("<img/>", {src: detail.img})
                )
                let ddocente= div.find("#desc-docente").eq(0)
                ddocente.append (
                  $("<p/>", {text: detail.nome})
                )
                ddocente.append (
                  $("<p/>", {text: detail.formacao})
                )
                ddocente.append (
                  $("<p/>", {text: detail.email})
                )
              

                div.appendTo (divDocentes)

              });
          }

     });

 
});